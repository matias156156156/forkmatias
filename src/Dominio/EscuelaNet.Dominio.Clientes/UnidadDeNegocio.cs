using EscuelaNet.Dominio.SeedWoork;
using System.Collections.Generic;
using System.Linq;

namespace EscuelaNet.Dominio.Clientes
{
    public class UnidadDeNegocio : Entity
    {
        public string RazonSocial { get; set; }

        public string ResponsableDeUnidad { get; set; }

        public string Cuit { get; set; }

        public string EmailResponsable { get; set; }

        public string TelefonoResponsable { get; set; }

        public IList<Direccion> Direcciones { get; private set; }

        public IList<Solicitud> Solicitudes { get; set; }

        private UnidadDeNegocio() { }

        public UnidadDeNegocio(string razonSocial, string responsable, string cuit, string email, string telefono, Direccion direccion)
        {
            this.RazonSocial = razonSocial ?? throw new System.ArgumentNullException(nameof(razonSocial));
            this.ResponsableDeUnidad = responsable ?? throw new System.ArgumentNullException(nameof(responsable));
            this.Cuit = cuit ?? throw new System.ArgumentNullException(nameof(cuit));
            this.EmailResponsable = email ?? throw new System.ArgumentNullException(nameof(email));
            this.TelefonoResponsable = telefono ?? throw new System.ArgumentNullException(nameof(telefono));
            AgregarDireccion(direccion);
        }

        public void AgregarDireccion(Direccion direccion)
        {
            if (this.Direcciones == null)
            {
                this.Direcciones = new List<Direccion>();
            }
            if (!this.Direcciones.Any(x => x.toString() == direccion.toString()))
            {
                this.Direcciones.Add(direccion);
            }
        }

        public void AgregarSolicitud(Solicitud solicitud)
        {
            if (this.Solicitudes == null)
            {
                this.Solicitudes = new List<Solicitud>();
            }
            if (!this.Solicitudes.Any(x => x.Titulo == solicitud.Titulo))
            {
                this.Solicitudes.Add(solicitud);
            }
        }

    }
}
