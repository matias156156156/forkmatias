﻿using EscuelaNet.Dominio.SeedWoork;
using System.Collections.Generic;
using System.Linq;

namespace EscuelaNet.Dominio.Clientes
{
    public class Cliente : Entity, IAggregateRoot
   {
        public string RazonSocial { get; set; }

        public string Email { get; set; } 

        public Categoria Categoria { get; set; }

        public IList<UnidadDeNegocio> Unidades { get; private set; } 

        private Cliente() { }

        public Cliente(string razonSocial, string email, Categoria categoria)
        {
            this.RazonSocial = razonSocial ?? throw new System.ArgumentNullException(nameof(razonSocial));
            this.Email = email ?? throw new System.ArgumentNullException(nameof(email));
            this.Categoria = categoria;
        }

        public void AgregarUnidad(UnidadDeNegocio unidad)
        {
            if (this.Unidades == null)
            {
                this.Unidades = new List<UnidadDeNegocio>();
            }
            if(!this.Unidades.Any(x => x.RazonSocial == unidad.RazonSocial))
            {
                this.Unidades.Add(unidad);
            }
            
        }

    }

}