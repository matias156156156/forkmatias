﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
 
    public class Programador : Entity, IAggregateRoot
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Legajo { get; set; }
        public string Dni { get; set; }
        public string Rol { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public EstadoDeDisponibilidad Disponibilidad { get; set; }
        public IList<Conocimiento> Skills { get; set; }

        public Programador(string nombre, string apellido, int legajo, string dni, string rol, DateTime fecnac)
        {
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Legajo = legajo;
            this.Dni = dni;
            this.Rol = rol;
            this.FechaNacimiento = fecnac;
            this.Disponibilidad = EstadoDeDisponibilidad.PartTime;
        }

        private Programador()
        {
            this.Disponibilidad = EstadoDeDisponibilidad.PartTime;
        }

        public void PushConocimiento(string nombre)
        {
            if (this.Skills == null)
            {
                this.Skills = new List<Conocimiento>();
            }
            this.Skills.Add(new Conocimiento(nombre));
        }

        public void CambiarDisponibilidad(EstadoDeDisponibilidad disponibilidad)
        {

            if (this.Disponibilidad == EstadoDeDisponibilidad.PartTime)
            {
                this.Disponibilidad = disponibilidad;
            }
            else if (this.Disponibilidad == EstadoDeDisponibilidad.FullTime && disponibilidad != EstadoDeDisponibilidad.PartTime)
            {
                this.Disponibilidad = disponibilidad;
            }            
        }

        public string ConsultarDisponibilidad()
        {
            return this.Disponibilidad.ToString();
        }

    }
}
